# Homework 6

Попадаем в систему без пароля

# Способ 1
```
Во время меню GRUB нажимаем е, что бы была возможность разово сконфигурировать загрузчик

В строчке, начинающейся с linux16,  в конце строки добавляем `init=/bin/sh`
```

```
В этой строчке указывается какое ядро грузить, где брать root раздел, где swap , язык и прочие параметры. 
Соответсвенно, после загрузки всех этих параметров, командой `init=/bin/sh ` оказываемся в оболочке sh.
Но, root файловая система смонтирована на read only. Для каких либо действий ее нужно перемонтировать командой 
`mount -o remount,rw /` на read write
Готово !
```


# Способ 2

```
Всё к той же строке начинающеся с linux16 добавляем команду rd.break -прерывание процесса загрузки.

Попадаем в emergency mode. можно было в строчке linux16 сразу примонтировать root раздел на чтение\запись заменой параметра ro на rw.
Либо повторить процедуру `mount -o remount,rw /`

Примонтировав раздел на запись, теперь при помощи chroot попадаем в рут файловую систему `chroot /sysroot/` , правим root пароль `passwd root` ... и вообще, делаем что хотим.
После изменения root пароля необходимо сказать SELinux, что надо обновить хеши паролей.(как я это понял) создаем в / файл .autorelabel  touch /.autorelabel 
перезагружаемся и можем логиниться .
```

# Способ 3 
```
Третий способ не вижу смысла описывать, т.к он является по сути комбинацией первого и второго способа 
 `rw init=/sysroot/bin/sh`- мы сразу же грузимся в раздел /sysroot/ , без emergency mode. предварительно смонтировав его на read-write
```

# Разница между 1 и 2 способом

1. В первом способе мы вызываем оболочку сразу после подгрузки всех необходимых модулей.
2. Во втором случае мы прерываем загрузку и попадаем в emergency mode, откуда с помощью chroot (временная смена корня) попадаем в /sysroot/ , в оболочку sh


# Переименование VG


`vgs `- узнаем название vg

`vgrename <current> <new>` - переименовываем vg 

 Правим /`etc/fstab, /etc/default/grub, /boot/grub2/grub.cfg`. Везде заменяем старое
 название на новое (внимательно, не пропускаем ничего !!!)
 
 пересоздаем initrd 
` mkinitrd -f -v /boot/initramfs-$(uname -r).img $(uname -r)`

```
*** Creating image file done ***
*** Creating initramfs image file '/boot/initramfs-3.10.0-862.2.3.el7.x86_64.img' done ***
```


перезагружаемся ... если все было сделано правильно , всё заработает :)


# Добавляем модуль dracut

Скрипты модулей хранятся в каталоге `/usr/lib/dracut/modules.d/`. 
.
Для того чтобы  добавить свой модуль создаем там папку с именем 01test:
`mkdir /usr/lib/dracut/modules.d/01test`


создаем два файла скрипта - один сборщик модуля, второй- непосредственно исполняемый скрипт.
`test.sh и module-setup.sh`
`
пересобираем модуль initrd


*  `dracut -f -v `
или 

*  `mkinitrd -f -v /boot/initramfs-$(uname -r).img $(uname -r)`



1.   Перезагрузиться и руками выключить опции rghb и quiet и увидеть вывод нового модуля (press E  во время загрузки)
2.   Либо отредактировать grub.cfg убрав эти опции


